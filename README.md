# Teste empresas-dotNET

Tecnologias implementadas:

  - ASP.NET Core 2.2 (com .NET Core)
  - EntityFramework
  - Swagger UI
  - IdentityServer4

### Urls
API | http://localhost:53320

Swagger | http://localhost:53320/swagger/index.html

Login | http://localhost:53320/api/v1/Users/auth/sign_in


### Observações

    para request nos endpoits abaixo e necessário somente o [access-token] retornado no cabecalho do response do login.
    
Listagem de Empresas | http://localhost:53320/api/v1/enterprises

Detalhamento de Empresas | http://localhost:53320/api/v1/Enterprises/{id}

Filtro de Empresas por nome e tipo http://localhost:53320/api/v1/enterprises/{enterprise_types}/{name}


### Usuário Teste ###

* Server: http://localhost:53320/
* API Version: v1
* Test User: bruno.max@outlook.com
* Test Password : 123456789