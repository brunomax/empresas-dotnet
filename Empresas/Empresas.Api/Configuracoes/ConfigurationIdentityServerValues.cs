﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresas.Api.Configuracoes
{
    public class ConfigurationIdentityServerValues
    {
        public string ApiName { get; set; }
        public string Secret { get; set; }
        public string ClientId { get; set; }
        public string EndPoint { get; set; }
    }
}
