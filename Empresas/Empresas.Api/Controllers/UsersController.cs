﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Empresas.Api.Configuracoes;
using Empresas.Dominio.Contratos;
using Empresas.Dominio.Entidades;
using IdentityModel.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Empresas.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {

        private readonly IAuthRepository _authRepository;
        private readonly IInvestorRepository _investorRepository;
        private readonly IOptions<ConfigurationIdentityServerValues> _configurationIdentityServerValues;

        public UsersController
        (
            IAuthRepository authRepository,
            IInvestorRepository investorRepository,
            IOptions<ConfigurationIdentityServerValues> options
        )
        {
            _authRepository = authRepository;
            _investorRepository = investorRepository;
            _configurationIdentityServerValues = options;

        }

        [HttpPost]
        [Route("auth/sign_in")]
        public async Task<IActionResult> Sign_in([FromBody]UserSecurity user)
        {
            try
            {
                bool authenticated = _authRepository.Sing_up(user.Email, user.Password);
                if (authenticated)
                {

                    var investor = _investorRepository.GetByEmail(user.Email);

                    var discovery = DiscoveryClient.GetAsync(_configurationIdentityServerValues.Value.EndPoint);
                    var tokenClient = new TokenClient(discovery.Result.TokenEndpoint, _configurationIdentityServerValues.Value.ClientId, _configurationIdentityServerValues.Value.Secret);
                    var tokenResponse = await tokenClient.RequestClientCredentialsAsync(_configurationIdentityServerValues.Value.ApiName);

                    if (tokenResponse.IsError)
                    {
                        return new JsonResult(tokenResponse.Error);
                    }
                                        
                    HttpContext.Response.Headers.Add("access-token", tokenResponse.AccessToken);
                    HttpContext.Response.Headers.Add("token-type", tokenResponse.TokenType);
                    HttpContext.Response.Headers.Add("expiry", tokenResponse.ExpiresIn.ToString());
                    HttpContext.Response.Headers.Add("uid", user.Email);
                    HttpContext.Response.Headers.Add("client", _configurationIdentityServerValues.Value.ClientId);

                    return Ok(
                        new
                        {
                            investor,
                            success = true
                        });
                }

                return Unauthorized(new
                {
                    success = false,
                    errors = "Invalid login credentials. Please try again."
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}