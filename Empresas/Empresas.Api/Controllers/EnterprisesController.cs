﻿using Empresas.Dominio.Contratos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Empresas.Api.Controllers
{

    [Route("api/v1/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseRepository _enterpriseRepository;

        public EnterprisesController
        (
            IEnterpriseRepository enterpriseRepository
        )
        {
            _enterpriseRepository = enterpriseRepository;
        }

        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            var teste = User;
            try
            {
                var enterprises = _enterpriseRepository.GetAll();
                if (enterprises.FirstOrDefault() == null)
                    return NotFound();
                return Ok(enterprises);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var enterprise = _enterpriseRepository.GetById(id);

                if (enterprise == null)
                    return NotFound();

                return Ok(enterprise);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpGet("{enterprise_types:int}/{name}")]
        public IActionResult GetByTypeName(int enterprise_types, string name)
        {

            try
            {
                var enterprise = _enterpriseRepository.Filter(enterprise_types, name);
                if (enterprise == null)
                    return NotFound();
                return Ok(enterprise);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}