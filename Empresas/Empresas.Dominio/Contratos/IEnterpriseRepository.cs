﻿using Empresas.Dominio.Entidades;
using System.Collections.Generic;

namespace Empresas.Dominio.Contratos
{
    public interface IEnterpriseRepository
    {
        IEnumerable<Enterprise> GetAll();
        Enterprise GetById(int id);
        Enterprise Filter(int enterprise_types, string name);
    }
}
