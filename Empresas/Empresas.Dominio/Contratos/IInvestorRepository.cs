﻿using Empresas.Dominio.Entidades;

namespace Empresas.Dominio.Contratos
{
    public interface IInvestorRepository
    {
        Investor GetByEmail(string email);
    }
}
