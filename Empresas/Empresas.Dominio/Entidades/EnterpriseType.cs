﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresas.Dominio.Entidades
{
    public class EnterpriseType
    {
        public int Id { get; private set; }
        public string Enterprise_type_name { get; set; }
    }
}
