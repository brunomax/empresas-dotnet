﻿using System.Collections.Generic;

namespace Empresas.Dominio.Entidades
{
    public class Portfolio
    {
        public int Id { get; private set; }
        public int Enterprises_number { get; private set; }
        public List<Enterprise> Enterprises { get; private set; }
    }
}