﻿namespace Empresas.Dominio.Entidades
{
    public class UserSecurity
    {
        public int Id { get; private set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
