﻿using System.Collections.Generic;

namespace Empresas.Dominio.Entidades
{
    public class Investor
    {
        public int Id { get; private set; }
        public string Investor_name { get; private set; }
        public string Email { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        public int Balance { get; private set; }
        public int PortfolioId { get; set; }
        public virtual Portfolio Portfolio { get; private set; }
        public int Portfolio_value { get; private set; }
        public bool First_access { get; private set; }
        public bool Super_angel { get; private set; }
        public int UserSecurityId { get; private set; }

    }
}
