﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresas.Dominio.Entidades
{
    public class Enterprise
    {
        public int Id { get; private set; }
        public string Email_enterprise { get; private set; }
        public string Facebook { get; private set; }
        public string Linkedin { get; private set; }
        public string Phone { get; private set; }
        public bool Own_enterprise { get; private set; }
        public string Enterprise_name { get; private set; }
        public string Photo { get; private set; }
        public string Description { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        public int Value { get; private set; }
        public decimal Share_price { get; private set; }

        public int EnterpriseTypeId { get; set; }
        public virtual EnterpriseType EnterpriseType { get; set; }

    }
}

