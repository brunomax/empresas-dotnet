﻿using Empresas.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;

namespace Empresas.Infra.Contexto
{
    public class DataContext : DbContext
    {

        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
        }

        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<EnterpriseType> EnterpriseType { get; set; }
        public DbSet<UserSecurity> UserSecurity { get; set; }
        public DbSet<Investor> Investor { get; set; }
    }
}
