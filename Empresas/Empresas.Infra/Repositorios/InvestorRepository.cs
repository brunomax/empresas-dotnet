﻿using Empresas.Dominio.Contratos;
using Empresas.Dominio.Entidades;
using Empresas.Infra.Contexto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Empresas.Infra.Repositorios
{
    public class InvestorRepository : IInvestorRepository
    {
        private readonly DataContext _context;

        public InvestorRepository(DataContext context)
        {
            _context = context;
        }

        public Investor GetByEmail(string email)
        {
            return _context.Investor
                .Include(x => x.Portfolio)
                .Include(x => x.Portfolio.Enterprises)
                .FirstOrDefault(x => x.Email == email);
        }
    }
}
