﻿using Empresas.Dominio.Contratos;
using Empresas.Infra.Contexto;
using System;
using System.Linq;

namespace Empresas.Infra.Repositorios
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;

        public AuthRepository(DataContext context)
        {
            _context = context;
        }
        public bool Sing_up(string email, string password)
        {
            return _context.UserSecurity
                .Any(x => x.Email == email && x.Password == password);
        }
    }
}
