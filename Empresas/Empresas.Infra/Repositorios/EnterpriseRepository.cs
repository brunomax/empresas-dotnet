﻿using Empresas.Dominio.Contratos;
using Empresas.Dominio.Entidades;
using Empresas.Infra.Contexto;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Empresas.Infra.Repositorios
{
    public class EnterpriseRepository : IEnterpriseRepository
    {
        private readonly DataContext _context;
        public EnterpriseRepository(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<Enterprise> GetAll()
        {
            return _context.Enterprise
                .Include(x => x.EnterpriseType);
        }

        public Enterprise GetById(int id)
        {
            return _context.Enterprise
                .Include(x => x.EnterpriseType)
                .FirstOrDefault(x => x.Id == id);
        }

        public Enterprise Filter(int enterprise_types, string name)
        {
            return _context.Enterprise
                .Include(x => x.EnterpriseType)
                .FirstOrDefault(x => x.EnterpriseType.Id == enterprise_types && x.Enterprise_name == name);
        }
    }
}
